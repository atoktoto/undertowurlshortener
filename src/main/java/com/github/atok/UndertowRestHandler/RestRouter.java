package com.github.atok.UndertowRestHandler;


import io.undertow.server.HttpServerExchange;
import org.apache.log4j.Logger;


public interface RestRouter {
    public Object executeRequest(HttpServerExchange exchange) throws RouteNotFoundException, RouteExecutionException;
    public void addRoute(Object resourceObject) throws RouteAddException;
    public void logRoutes(Logger logger);

    public static class RouteNotFoundException extends Exception {
        public RouteNotFoundException() {
            super();
        }

        public RouteNotFoundException(Throwable cause) {
            super(cause);
        }
    }
    public static class RouteAddException extends Exception {
        public RouteAddException() {
            super();
        }

        public RouteAddException(Throwable cause) {
            super(cause);
        }
    }


    public static class RouteExecutionException extends Exception {
        public RouteExecutionException() {
            super();
        }

        public RouteExecutionException(Throwable cause) {
            super(cause);
        }
    }
}
