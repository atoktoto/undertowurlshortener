package com.github.atok.UndertowRestHandler.runtimeCompile;


import com.github.atok.UndertowRestHandler.reflection.RestRoute;

public class CompiledRoute {
    public final CompiledRouteLauncher launcher;
    public final RestRoute route;

    public CompiledRoute(CompiledRouteLauncher launcher, RestRoute route) {
        this.launcher = launcher;
        this.route = route;
    }
}
