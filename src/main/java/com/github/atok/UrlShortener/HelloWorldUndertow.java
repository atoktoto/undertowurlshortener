package com.github.atok.UrlShortener;


import com.github.atok.UrlShortener.handlers.ErrorHandler;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

    public class HelloWorldUndertow {
        public static void main(String[] args) {
            Undertow undertow = Undertow.builder()
                    .addHttpListener(9090, "0.0.0.0")
                    .setHandler(new HttpHandler() {
                        @Override
                        public void handleRequest(HttpServerExchange exchange) throws Exception {
                            exchange.setResponseCode(200);
                            exchange.getResponseSender().send("hello!");
                        }
                    }).build();
            undertow.start();
        }
    }





