package com.github.atok.UrlShortener.handlers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.atok.UndertowRestHandler.DefaultResponseSender;
import com.github.atok.UndertowRestHandler.Response;
import com.github.atok.UrlShortener.model.Error;
import io.undertow.Handlers;
import io.undertow.server.DefaultResponseListener;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.ResponseCodeHandler;
import io.undertow.util.StatusCodes;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;



public class ErrorHandler implements HttpHandler {
    private volatile HttpHandler next = ResponseCodeHandler.HANDLE_404;

    /**
     * The response codes that this handler will handle. If this is null then it will handle all 4xx and 5xx codes.
     */
    private volatile Set<Integer> responseCodes = null;

    public ErrorHandler(final HttpHandler next) {
        this.next = next;
    }

    public ErrorHandler() {
    }



    @Override
    public void handleRequest(final HttpServerExchange exchange) throws Exception {
        exchange.addDefaultResponseListener(new DefaultResponseListener() {
            @Override
            public boolean handleDefaultResponse(final HttpServerExchange exchange) {
                if (!exchange.isResponseChannelAvailable()) {
                    return false;
                }
                Set<Integer> codes = responseCodes;
                if (codes == null ? exchange.getResponseCode() >= 400 : codes.contains(Integer.valueOf(exchange.getResponseCode()))) {
                    try {
                        final Error error = new Error("No handler was found to handle your request.", null, exchange.getRequestURL());
                        DefaultResponseSender.send(exchange, new Response().error(StatusCodes.NOT_FOUND, error));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();    //TODO logger
                    }
                    return true;
                }
                return false;
            }
        });

        try {
            next.handleRequest(exchange);
        } catch (Exception e) {
            if(exchange.isResponseChannelAvailable()) {
                final Error error = new Error("Unknown error", ExceptionUtils.getStackTrace(e.getCause()), exchange.getRequestURL());
                Logger.getRootLogger().error("Error handling request", e);
                DefaultResponseSender.send(exchange, new Response().error(StatusCodes.INTERNAL_SERVER_ERROR, error));
            }
        }
    }

    public HttpHandler getNext() {
        return next;
    }

    public ErrorHandler setNext(final HttpHandler next) {
        Handlers.handlerNotNull(next);
        this.next = next;
        return this;
    }

    public Set<Integer> getResponseCodes() {
        return Collections.unmodifiableSet(responseCodes);
    }

    public ErrorHandler setResponseCodes(final Set<Integer> responseCodes) {
        this.responseCodes = new HashSet<Integer>(responseCodes);
        return this;
    }

    public ErrorHandler setResponseCodes(final Integer... responseCodes) {
        this.responseCodes = new HashSet<Integer>(Arrays.asList(responseCodes));
        return this;
    }
}
