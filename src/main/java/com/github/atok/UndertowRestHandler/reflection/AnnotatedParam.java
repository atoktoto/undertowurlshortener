package com.github.atok.UndertowRestHandler.reflection;

import java.lang.annotation.Annotation;


public class AnnotatedParam {
    public final Annotation[] annotations;
    public final Class<?> cls;

    public AnnotatedParam(Annotation[] annotations, Class<?> cls) {
        this.annotations = annotations;
        this.cls = cls;
    }
}
