package com.github.atok.UndertowRestHandler;

import io.undertow.util.StatusCodes;


public class Response {

    public String contentType;
    public Object content;
    public int statusCode;

    public Response contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public Response statusCode(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public Response content(Object content) {
        this.content = content;
        return this;
    }

    public Response ok() {
        statusCode(StatusCodes.OK);
        return this;
    }

    public Response ok(Object object) {
        this.statusCode = StatusCodes.OK;
        this.content = object;
        return this;
    }

    public Response error(int code, Object object) {
        this.statusCode = code;
        this.content = object;
        return this;
    }
}
