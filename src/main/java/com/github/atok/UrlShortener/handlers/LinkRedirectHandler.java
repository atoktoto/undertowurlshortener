package com.github.atok.UrlShortener.handlers;

import com.github.atok.UrlShortener.LinkRepository;
import com.github.atok.UrlShortener.model.Link;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;


public class LinkRedirectHandler implements HttpHandler {

    final LinkRepository linkRepository;
    public LinkRedirectHandler(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        String id = null;
        String relativePath = exchange.getRelativePath();
        if(relativePath.startsWith("/") && relativePath.length() > 1) {
            id = relativePath.substring(1);
        }

        if(id != null) {
            Link link = linkRepository.getLink(id);
            if(link != null) {
                redirectTo(exchange, link.stored.target);
                linkRepository.countClick(link);
            } else {
                notFound(exchange);
            }
        } else {
            notFound(exchange);
        }
    }

    private void notFound(HttpServerExchange exchange) {
        exchange.setResponseCode(StatusCodes.NOT_FOUND);
        exchange.endExchange();
    }

    private void redirectTo(HttpServerExchange exchange, String url) {
        exchange.setResponseCode(StatusCodes.FOUND);
        exchange.getResponseHeaders().put(Headers.LOCATION, url);
        exchange.endExchange();
    }
}
