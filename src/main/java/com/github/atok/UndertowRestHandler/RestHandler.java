package com.github.atok.UndertowRestHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.github.atok.UndertowRestHandler.reflection.ReflectionRestRouter;
import com.github.atok.UndertowRestHandler.runtimeCompile.RuntimeCompilingRestRouter;
import com.github.atok.UrlShortener.model.Error;
import io.undertow.io.Sender;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;
import org.apache.log4j.Logger;

/**
 * Supported annotations are Path, all request types (GET, DELETE, POST, PUT, HEAD). <br>
 * <br>
 * Biggest differences: <br>
 *    - Resource classes are created once (you call the constructor) and provided to ReflectionRestHandler. They have to be thread-safe. <br>
 *    - The only injected params are Strings annotated with PathParam and HttpServerExchange.
 *    - This Handler will not catch any exceptions thrown by resources. <br>
 *    - Does not implement default HEAD and OPTIONS methods. <br>
 */
public class RestHandler implements HttpHandler {

    public static final ObjectReader objectReader;
    public static final ObjectWriter objectWriter;
    public final RestRouter router;

    static {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectReader = objectMapper.reader();
        objectWriter = objectMapper.writer();
    }

    public RestHandler(Object... resourceObjects) throws RestRouter.RouteAddException {
        router = new ReflectionRestRouter();
//        router = new RuntimeCompilingRestRouter();
        for (Object resourceObject : resourceObjects) {
            router.addRoute(resourceObject);
        }
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        try {
            Object result = router.executeRequest(exchange);
            if(result != null) {
                sendResponse(exchange, result);
            }
        } catch (RestRouter.RouteNotFoundException e) {
            sendResponse(exchange, new Response().error(StatusCodes.NOT_FOUND, new Error("Method not found", null, exchange.getRequestURL())));
        }
    }

    private void sendResponse(HttpServerExchange exchange, Object response) throws JsonProcessingException {
        if(response instanceof String) {
            String content = (String)response;
            exchange.getResponseHeaders().put(Headers.CONTENT_LENGTH, "" + content.length());
            Sender sender = exchange.getResponseSender();
            sender.send(content);
        } else if(response instanceof Response) {
            DefaultResponseSender.send(exchange, (Response) response);  //TODO plugins
        } else {
            DefaultResponseSender.send(exchange, new Response().ok(response));
        }
    }

    public void logRoutes() {
        router.logRoutes(Logger.getRootLogger());
    }



}
