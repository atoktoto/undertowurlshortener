package com.github.atok.UrlShortener.model;

public class Error {
    public final String message;
    public final String stackTrace;
    public final String requestUrl;

    public Error(String message, String stackTrace, String requestUrl) {
        this.message = message;
        this.stackTrace = stackTrace;
        this.requestUrl = requestUrl;
    }

    @Override
    public String toString() {
        return "ErrorMessage{" +
                "message='" + message + '\'' +
                ", stackTrace='" + stackTrace + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                '}';
    }
}
