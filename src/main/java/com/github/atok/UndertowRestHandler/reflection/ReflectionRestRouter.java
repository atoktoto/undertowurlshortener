package com.github.atok.UndertowRestHandler.reflection;


import com.github.atok.UndertowRestHandler.RestRouter;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.util.HttpString;
import org.apache.log4j.Logger;
import org.xnio.Pooled;

import javax.ws.rs.*;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReflectionRestRouter implements RestRouter {

    List<RestRoute> mappingList = new ArrayList<>();

    @Override
    public Object executeRequest(HttpServerExchange exchange) throws RouteNotFoundException {
        final HttpString method = exchange.getRequestMethod();
        final String relativePath = exchange.getRelativePath();

        RestRoute foundMapping = null;
        Map<String, String> map = new HashMap<>();

        for (RestRoute restRoute : mappingList) {
            boolean templateMatch = restRoute.uriTemplate.match(relativePath, map);
            boolean methodMatch = method.equals(restRoute.httpMethod);

            if(templateMatch && methodMatch) {
                foundMapping = restRoute;
                break;
            }
        }

        if(foundMapping != null) {
            try {
                return callMappingMethod(exchange, foundMapping, map);
            } catch (InvocationTargetException | IllegalAccessException | IOException e) {
                throw new RuntimeException(e);  //FIXME proper exception
            }
        } else {
            throw new RouteNotFoundException();
        }
    }

    @Override
    public void addRoute(Object resourceObject) { //TODO move to builder (to make it immutable)
        mappingList.addAll(RestRoute.fromObject(resourceObject));
    }

    @Override
    public void logRoutes(org.apache.log4j.Logger logger) {
        StringBuilder sb = new StringBuilder();
        sb.append("Mappings of ");
        sb.append(this);
        sb.append(": \n");
        for (RestRoute restRoute : mappingList) {
            String urlDescription = String.format("%4s %s", restRoute.httpMethod, restRoute.uriTemplate);
            String methodString = String.format("  %-30s -> %s:%s", urlDescription, restRoute.resourceObject.getClass(), restRoute.methodToCall.getName());
            sb.append(methodString);
            sb.append("\n");
        }

        logger.info(sb.toString());
    }


    private Object callMappingMethod(HttpServerExchange exchange, RestRoute mapping, Map<String, String> argumentMap) throws InvocationTargetException, IllegalAccessException, IOException {
        Object[] params = new Object[mapping.methodParams.length];

        for (int i = 0; i < mapping.methodParams.length; i++) {
            AnnotatedParam param = mapping.methodParams[i];
            Class<?> expectedType = param.cls;

            if(expectedType == HttpServerExchange.class) {
                params[i] = exchange;
            } else {
                String defaultValue = null;
                for (Annotation annotation : param.annotations) {
                    if(annotation instanceof FormParam){
                        String paramName = ((FormParam) annotation).value();
                        FormData formData = exchange.getAttachment(FormDataParser.FORM_DATA);
                        if(formData != null && formData.contains(paramName)) {
                            params[i] = formData.get(paramName).getFirst().getValue();
                        }
                    } else if(annotation instanceof PathParam) {
                        String paramName = ((PathParam) annotation).value();
                        if(argumentMap.containsKey(paramName)) {
                            params[i] = argumentMap.get(paramName);
                        }
                    } else if(annotation instanceof DefaultValue) {
                        defaultValue = ((DefaultValue) annotation).value();
                    }
                }
                if(params[i] == null) {
                    params[i] = defaultValue;
                }
            }

            if(params[i] == null) {
                Logger.getRootLogger().error("Unknown param: " + params[i] + " . Setting to null.");
            }
        }

        Object result = mapping.methodToCall.invoke(mapping.resourceObject, params);
        return result;
    }



}
