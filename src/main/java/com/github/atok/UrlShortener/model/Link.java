package com.github.atok.UrlShortener.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Link {
    public final String shortUrl;
    public final String viewUrl;

    @JsonIgnore
    public final StoredLink stored;

    public Link(StoredLink storedLink, String shortUrlBase, String viewUrlBase) {
        this.stored = storedLink;
        this.viewUrl = viewUrlBase + storedLink.id;
        this.shortUrl = shortUrlBase + storedLink.id;
    }

    public String getId() {
        return stored.id;
    }

    public String getTargetUrl() {
        return stored.target;
    }
}
