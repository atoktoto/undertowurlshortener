package com.github.atok.UrlShortener.model;


import java.io.Serializable;

public class StoredLink implements Serializable {

    public String target;
    public String id;
    public String secret;
    public volatile int clickCount;

    public StoredLink(String target, String id, String secret, int clickCount)  {
        this.id = id;
        this.target = target;
        this.secret = secret;
        this.clickCount = clickCount;
    }

    @Override
    public String toString() {
        return "Link[target = " + target + "]";
    }
}
