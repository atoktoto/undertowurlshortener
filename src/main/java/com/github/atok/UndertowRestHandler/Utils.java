package com.github.atok.UndertowRestHandler;


public class Utils {
    public static String ensureStartingSlash(String path) {
        if(!path.startsWith("/")) {
            path = "/" + path;
        }
        return path;
    }

    public static String ensureEndingSlash(String path) {
        if(!path.endsWith("/")) {
            path = path + "/";
        }
        return path;
    }

    public static String ensureNoStartingSlash(String path) {
        if(path.startsWith("/")) {
            path = path.substring(1);
        }
        return path;
    }
}
