package com.github.atok.UrlShortener.resources;

import com.github.atok.UrlShortener.LinkRepository;
import com.github.atok.UrlShortener.VelocityRenderer;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.resource.ResourceHandler;
import org.apache.velocity.VelocityContext;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.io.StringWriter;

@Path("/")
public class ViewLinkResource {

    private final LinkRepository linkRepository;
    private final ResourceHandler resourceHandler;
    private final VelocityRenderer velocityRenderer = new VelocityRenderer();

    public ViewLinkResource(LinkRepository linkRepository, ResourceHandler resourceHandler) {
        this.linkRepository = linkRepository;
        this.resourceHandler = resourceHandler;
    }

    @GET
    @Path("{id}")
    public String getLink(HttpServerExchange exchange, @PathParam("id") String id) throws Exception {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("test", "loltest");

        StringWriter sw = new StringWriter();
        velocityRenderer.render("test.vm", velocityContext, sw);

        return sw.toString();
    }
}
