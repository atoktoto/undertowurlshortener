package com.github.atok.UrlShortener.resources;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.atok.UndertowRestHandler.Response;
import com.github.atok.UrlShortener.LinkRepository;
import com.github.atok.UrlShortener.model.Error;
import com.github.atok.UrlShortener.model.Link;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;

import javax.ws.rs.*;
import java.util.Deque;

@Path("link")
public class LinkResource {

    private final LinkRepository linkRepository;
    public LinkResource(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    @GET
    public Response getAll(HttpServerExchange exchange) throws Exception {
        return new Response().ok(linkRepository.getAll());
    }

    @GET
    @Path("/echo/{text}")
    public Response getTest(@PathParam("text") String text) {
        return new Response().ok("Echo: " + text);
    }

    @GET
    @Path("{id}")
    public Response getLink(HttpServerExchange exchange, @PathParam("id") String id) throws Exception {
        return new Response().ok(linkRepository.getLink(id));
    }

    @POST
    public Response createNewLinkFromQueryParam(HttpServerExchange exchange, @QueryParam("url") @FormParam("url") String url) throws Exception {
        if(url != null) {
            return new Response().ok(createNewLink(url));
        } else {
            return new Response().error(StatusCodes.BAD_REQUEST, new Error("Missing 'url' parameter", null, exchange.getRequestURL()));
        }
    }

    @POST
    @Path("{url}")
    public Response post(HttpServerExchange exchange, @FormParam("url") @PathParam("url") String url) throws Exception {
        if(url != null) {
            return new Response().ok(createNewLink(url));
        } else {
            return new Response().error(StatusCodes.BAD_REQUEST, new Error("Missing 'url' parameter", null, exchange.getRequestURL()));
        }
    }

    private Link createNewLink(String targetUrl) throws JsonProcessingException {
        if(!targetUrl.startsWith("http://") && !targetUrl.startsWith("https://")) {
            targetUrl = "http://" + targetUrl;
        }
        return linkRepository.createLink(targetUrl);
    }
}
