package com.github.atok.UrlShortener;

import com.github.atok.UndertowRestHandler.RestHandler;
import com.github.atok.UndertowRestHandler.RestRouter;
import com.github.atok.UrlShortener.handlers.ErrorHandler;
import com.github.atok.UrlShortener.handlers.LinkRedirectHandler;
import com.github.atok.UrlShortener.model.StoredLink;
import com.github.atok.UrlShortener.resources.LinkResource;
import com.github.atok.UrlShortener.resources.ViewLinkResource;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.accesslog.AccessLogHandler;
import io.undertow.server.handlers.accesslog.AccessLogReceiver;
import io.undertow.server.handlers.form.EagerFormParsingHandler;
import io.undertow.server.handlers.resource.FileResourceManager;
import io.undertow.server.handlers.resource.ResourceHandler;
import net.openhft.chronicle.map.ChronicleMapBuilder;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ShortenerService {

    final HttpHandler rootHandler;

    public ShortenerService(boolean persistLinks) throws IOException, ServletException, RestRouter.RouteAddException {

        Map<String, StoredLink> map;
        if(persistLinks) {
            map = ChronicleMapBuilder.of(String.class, StoredLink.class)
                    .createPersistedTo(new File("linkstorage.hmap"));
        } else {
            map = Collections.synchronizedMap(new HashMap<>());
        }

        FileResourceManager fileResourceManager = new FileResourceManager(new File("static/"), 100);
        ResourceHandler resourceHandler = Handlers.resource(fileResourceManager)
                .addWelcomeFiles("index.html");
        resourceHandler.setCanonicalizePaths(true);
        resourceHandler.setCacheTime(6000);
        resourceHandler.setCachable(value -> true);

        LinkRepository linkRepository = new LinkRepository(map, "http://localhost:9090/r/", "http://localhost:9090/view/");


        ViewLinkResource viewLinkResource = new ViewLinkResource(linkRepository, resourceHandler);
        RestHandler viewHandler = new RestHandler(viewLinkResource);
        viewHandler.logRoutes();

        LinkResource linkResource = new LinkResource(linkRepository);
        RestHandler apiHandler = new RestHandler(linkResource);
        apiHandler.logRoutes();

        LinkRedirectHandler linkRedirectHandler = new LinkRedirectHandler(linkRepository);



        HttpHandler routingHandler = Handlers.path()
                .addPrefixPath("api", apiHandler)
                .addPrefixPath("r", linkRedirectHandler)
                .addPrefixPath("view", viewHandler)
                .addPrefixPath("/", resourceHandler);

        EagerFormParsingHandler formParsingHandler = new EagerFormParsingHandler();
        formParsingHandler.setNext(routingHandler);

        rootHandler = formParsingHandler;
    }

    public HttpHandler getRootHandler() {
        return rootHandler;
    }

    private static void setUpLogging() {
        ConsoleAppender console = new ConsoleAppender(); //create appender
        //configure the appender
        String PATTERN = "%d [%p|%c|%C{1}] %m%n";
        console.setLayout(new PatternLayout(PATTERN));
        console.setThreshold(Level.INFO);
        console.activateOptions();
        //add appender to any Logger (here is root)
        Logger.getRootLogger().addAppender(console);
    }


    public static void main(String[] args) throws IOException, ServletException, RestRouter.RouteAddException {
        setUpLogging();
        final Logger log = Logger.getRootLogger();

        AccessLogReceiver logReceiver = message -> log.info(message);

        Undertow undertow = Undertow.builder()
                .setWorkerThreads(20)
                .addHttpListener(9090, "0.0.0.0")
                .setHandler(
                        new ErrorHandler(
                                new AccessLogHandler(
                                        new ShortenerService(true).getRootHandler()
                                        , logReceiver, "common", logReceiver.getClass().getClassLoader()
                                )
                        ))
                .build();

        undertow.start();
    }


}
