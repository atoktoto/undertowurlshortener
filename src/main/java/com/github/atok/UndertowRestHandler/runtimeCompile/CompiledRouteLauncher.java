package com.github.atok.UndertowRestHandler.runtimeCompile;

public interface CompiledRouteLauncher {
    public Object execute(Object... params) throws Exception;
}
