package com.github.atok.UndertowRestHandler;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.undertow.io.Sender;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class DefaultResponseSender {

    private static final ObjectReader objectReader;
    private static final ObjectWriter objectWriter;

    static {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectReader = objectMapper.reader();
        objectWriter = objectMapper.writer();
    }

    public static String serialize(Object object) throws JsonProcessingException {
        return objectWriter.withRootName(object.getClass().getSimpleName().toLowerCase()).writeValueAsString(object);
    }

    public static void send(HttpServerExchange exchange, Response response) throws JsonProcessingException {
        if(response.content == null) {
            return;
        }

        String content = serialize(response.content);

        if(response.contentType == null) {
            response.contentType = "application/json";
        }

        exchange.getResponseHeaders().put(Headers.CONTENT_LENGTH, "" + content.length());
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, response.contentType);
        exchange.setResponseCode(response.statusCode);
        Sender sender = exchange.getResponseSender();
        sender.send(content);
    }


}
