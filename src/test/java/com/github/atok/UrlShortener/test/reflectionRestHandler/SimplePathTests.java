package com.github.atok.UrlShortener.test.reflectionRestHandler;

import com.github.atok.UndertowRestHandler.RestHandler;
import com.github.atok.UndertowRestHandler.RestRouter;
import com.github.atok.UrlShortener.test.UndertowTestBase;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.undertow.Handlers;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.log4j.Level;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import static org.junit.Assert.assertTrue;

public class SimplePathTests extends UndertowTestBase {

    @BeforeClass
    public static void configureLogging() {
        configureLog4j(Level.DEBUG);
    }

    @Before
    public void start() throws RestRouter.RouteAddException {
        RestHandler handler = new RestHandler(new Test1(), new Resource2());
        HttpHandler routing = Handlers.path()
                .addPrefixPath("/tested", handler);
        startUndertowWithHandler(routing);
    }

    @After
    public void stop() {
        stopUndertow();
    }

    @Test
    public void simplePathTest() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/test1/lol").asString();
        assertTrue("The response code should be 200, but is: " + response.getCode(), response.getCode() == 200);
    }

    @Test
    public void pathWithParam() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/test1/lol?pram=param_value").asString();
        assertTrue("The response code should be 200, but is: " + response.getCode(), response.getCode() == 200);
    }

    @Test
    public void pathWithExtraElement() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/test1/lol/somethingextra").asString();
        assertTrue("The response code should be 404, but is: " + response.getCode(), response.getCode() == 404);
    }

    @Test
    public void simplePathTest404() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/test1/lol657").asString();
        assertTrue("The response code should be 404, but is: " + response.getCode(), response.getCode() == 404);
    }

    @Test
    public void assumedSlashes() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/resource2/method1").asString();
        assertTrue("The response code should be 200, but is: " + response.getCode(), response.getCode() == 200);
    }

    @Test
    public void urlEncodedPath() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/resource2/met%20hod2").asString();
        assertTrue("The response code should be 200, but is: " + response.getCode(), response.getCode() == 200);
        assertTrue("It's not a right method", response.getBody().equals("method2"));
    }

    @Test
    public void noPathMethod() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/resource2").asString();
        assertTrue("The response code should be 200, but is: " + response.getCode(), response.getCode() == 200);
        assertTrue("It's not a right method", response.getBody().equals("method3"));
    }

    @Path("/test1")
    public static class Test1 {
        @GET
        @Path("/lol")
        public String hello(HttpServerExchange exchange) {
            return "hello";
        }
    }

    @Path("resource2")
    public static class Resource2 {
        @GET
        @Path("method1")
        public String method1(HttpServerExchange exchange) {
            return "method1";
        }

        @GET
        @Path("met hod2")
        public String method2() {
            return "method2";
        }

        @GET
        public String method3() {
            return "method3";
        }
    }

}
