package com.github.atok.UrlShortener.test;


import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import java.net.BindException;

public abstract class UndertowTestBase {

    Undertow undertow;

    protected void startUndertowWithHandler(HttpHandler handler)  {
        undertow = Undertow.builder()
                .addHttpListener(9090, "0.0.0.0")
                .setHandler(handler)
                .build();
        try {
            undertow.start();
        } catch (Exception e) {
            if(e.getCause() instanceof BindException) {
                Logger.getRootLogger().warn("Waiting for previous instance to close...");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                undertow.start();
            } else {
                throw e;
            }
        }
    }

    protected void stopUndertow() {
        if(undertow != null) {
            undertow.stop();
        }
    }

    protected static void configureLog4j(Level level) {
        ConsoleAppender console = new ConsoleAppender(); //create appender
        //configure the appender
        String PATTERN = "%d [%p|%c|%C{1}] %m%n";
        console.setLayout(new PatternLayout(PATTERN));
        console.setThreshold(level);
        console.activateOptions();
        //add appender to any Logger (here is root)
        Logger.getRootLogger().addAppender(console);
    }
}
