package com.github.atok.UrlShortener;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;

import java.io.StringWriter;

public class VelocityRenderer {

    private final VelocityEngine velocity;

    public VelocityRenderer() {
        velocity = new VelocityEngine();
        velocity.setProperty( RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS,
                "org.apache.velocity.runtime.log.Log4JLogChute" );

        velocity.setProperty("runtime.log.logsystem.log4j.logger", "velocity");
        velocity.init();
    }

    public void render(String templateName, VelocityContext context, StringWriter output) {
        Template template = velocity.getTemplate("templates/" + templateName, "UTF-8");
        template.merge(context, output);
    }

}

