package com.github.atok.UrlShortener.test.reflectionRestHandler;

import com.github.atok.UndertowRestHandler.RestHandler;
import com.github.atok.UndertowRestHandler.RestRouter;
import com.github.atok.UrlShortener.test.UndertowTestBase;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.undertow.Handlers;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.log4j.Level;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import static org.junit.Assert.assertTrue;


public class PathParamTests extends UndertowTestBase {

    @BeforeClass
    public static void configureLogging() {
        configureLog4j(Level.DEBUG);
    }

    @Before
    public void start() throws RestRouter.RouteAddException {
        RestHandler handler = new RestHandler(new Test2());
        HttpHandler routing = Handlers.path()
                .addPrefixPath("/tested", handler);
        startUndertowWithHandler(routing);
    }

    @After
    public void stop() {
        stopUndertow();
    }

    @Test
    public void simplePathTest() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/test2/lol").asString();
        assertTrue("The response code should be 404, but is: " + response.getCode(), response.getCode() == 404);
    }

    @Test
    public void pathWithParam() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/test2/lol/someid?pram=param_value").asString();
        assertTrue("The response code should be 200, but is: " + response.getCode(), response.getCode() == 200);
    }

    @Test
    public void pathWithExtraElement() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/test2/lol/someid").asString();
        assertTrue("The response code should be 200, but is: " + response.getCode(), response.getCode() == 200);
        assertTrue("We didn't got the ID back. Got: " + response.getBody(), response.getBody().equals("id: someid"));
    }

    @Test
    public void simplePathTest404() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/test2/lol657").asString();
        assertTrue("The response code should be 404, but is: " + response.getCode(), response.getCode() == 404);
    }

    @Test
    public void pathWithDefaultParam() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/test2/method2").asString();
        assertTrue("The response code should be 200, but is: " + response.getCode(), response.getCode() == 200);
        assertTrue("We didn't got the default ID back. Got: " + response.getBody(), response.getBody().equals("id: none"));
    }

    @Test
    public void regexpParamExtension() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:9090/tested/test2/method3/part1/part2").asString();
        assertTrue("The response code should be 200, but is: " + response.getCode(), response.getCode() == 200);
        assertTrue("We didn't get the parameter back", response.getBody().equals("param1: part1/part2"));
    }

    @Path("/test2")
    public static class Test2 {
        @GET
        @Path("/lol/{id}")
        public String hello(HttpServerExchange exchange, @PathParam("id") String id) {
            return "id: " + id;
        }

        @GET
        @Path("/method2")
        public String method2(@DefaultValue("none") @PathParam("id") String id) {
            return "id: " + id;
        }

        @GET
        @Path("method3/{param1:.+}")
        public String method3(@PathParam("param1") String param1) {
            return "param1: " + param1;
        }
    }


}
