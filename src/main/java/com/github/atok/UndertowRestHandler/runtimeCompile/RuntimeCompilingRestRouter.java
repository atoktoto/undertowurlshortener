package com.github.atok.UndertowRestHandler.runtimeCompile;

import com.github.atok.UndertowRestHandler.RestRouter;
import com.github.atok.UndertowRestHandler.reflection.AnnotatedParam;
import com.github.atok.UndertowRestHandler.reflection.RestRoute;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import net.openhft.compiler.CachedCompiler;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.PathParam;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * DO NOT USE. It is just an experiment.
 */
@Deprecated
public class RuntimeCompilingRestRouter implements RestRouter {

    List<CompiledRoute> routeList = new ArrayList<>();
    private final CachedCompiler compiler = new CachedCompiler(null, null);

    @Override
    public Object executeRequest(HttpServerExchange exchange) throws RouteNotFoundException, RouteExecutionException {
        final HttpString method = exchange.getRequestMethod();
        final String relativePath = exchange.getRelativePath();
        final Map<String, String> map = new HashMap<>();

        Object result = null;
        for(CompiledRoute route : routeList) {
            boolean matched = route.route.uriTemplate.match(relativePath, map) && route.route.httpMethod.equals(method);
            if(matched) {
                result = callMappingMethod(exchange, route, map);
                break;
            }
        }

        if(result == null) {
            throw new RouteNotFoundException();
        } else {
            return result;
        }
    }

    @Override
    public void addRoute(Object resourceObject) throws RouteAddException {
        List<RestRoute> restRoutes = RestRoute.fromObject(resourceObject);
        for(RestRoute route : restRoutes) {
            String className = generateClassName(route);
            String classSource = generateClassSource(route, className);
            try {
                Logger.getRootLogger().warn("Source: \n" + classSource);
                Class cls = compiler.loadFromJava("com.github.atok.UndertowRestHandler.runtimeCompile." + className, classSource);
                CompiledRouteLauncher launcher = (CompiledRouteLauncher)cls.getConstructor(Object.class).newInstance(resourceObject);
                routeList.add(new CompiledRoute(launcher, route));
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                throw new RouteAddException(e);
            }
        }
    }

    private Object callMappingMethod(HttpServerExchange exchange, CompiledRoute route, Map<String, String> argumentMap) throws RouteExecutionException {
        Object[] params = new Object[route.route.methodParams.length];

        for (int i = 0; i < route.route.methodParams.length; i++) {
            AnnotatedParam param = route.route.methodParams[i];
            Class<?> expectedType = param.cls;

            if(expectedType == HttpServerExchange.class) {
                params[i] = exchange;
            } else {
                String defaultValue = null;
                for (Annotation annotation : param.annotations) {
                    if(annotation instanceof PathParam) {
                        String paramName = ((PathParam) annotation).value();
                        params[i] = argumentMap.get(paramName);
                    } else if(annotation instanceof DefaultValue) {
                        defaultValue = ((DefaultValue) annotation).value();
                    }
                }
                if(params[i] == null) {
                    params[i] = defaultValue;
                }
            }

            if(params[i] == null) {
                Logger.getRootLogger().error("Unknown param: " + params[i] + " . Setting to null.");
            }
        }

//        Logger.getRootLogger().info("Handling with: " + mapping.methodToCall);
        Object result = null;
        try {
            result = route.launcher.execute(params);
        } catch (Exception e) {
            throw new RouteExecutionException(e); //FIXME ??
        }
        return result;
    }

    private String generateClassName(RestRoute route) {
        return "CompiledRoute" + RandomStringUtils.randomAlphanumeric(6);        //FIXME better naming scheme
    }

    private String generateClassSource(RestRoute route, String className) {
        StringBuilder sb = new StringBuilder();
        String resourceClassName = route.resourceObject.getClass().getCanonicalName();
        String resourceMethodName = route.methodToCall.getName();

        String resourceMethodParams =
                IntStream.range(0, route.methodParams.length)
                .mapToObj(i -> "(" + route.methodParams[i].cls.getName() + ")params[" + i + "]")
                .collect(Collectors.joining(", "));

        sb.append("package com.github.atok.UndertowRestHandler.runtimeCompile;\n");

        sb.append("public class " + className + " implements CompiledRouteLauncher {\n");
        sb.append("    final " + resourceClassName + " resource;\n");
        sb.append("    public " + className + "(Object resource) {\n");
        sb.append("        this.resource = (" + resourceClassName + ")resource;\n");
        sb.append("    }\n");
        sb.append("    public Object execute(Object... params) throws Exception {\n");
        sb.append("        return resource." + resourceMethodName + "(" + resourceMethodParams + ");\n");
        sb.append("    }\n}\n");

        return sb.toString();
    }



    @Override
    public void logRoutes(Logger logger) {
        logger.info("Sorry. Not implemented.");
    }
}
