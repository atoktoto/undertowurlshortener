package com.github.atok.UrlShortener.test;

import com.github.atok.UndertowRestHandler.RestRouter;
import com.github.atok.UrlShortener.ShortenerService;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.log4j.Level;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.ServletException;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BasicFunctionalityTest extends UndertowTestBase {

    @BeforeClass
    public static void configureLogging() {
        configureLog4j(Level.DEBUG);
    }

    @Before
    public void start() throws IOException, ServletException, RestRouter.RouteAddException {
        startUndertowWithHandler(new ShortenerService(false).getRootHandler());
    }

    @After
    public void stop() {
        stopUndertow();
    }

    @Test
    public void createLinkAndFollowIt() throws UnirestException {
        HttpResponse<JsonNode> createResponse = Unirest.post("http://localhost:9090/api/link/www.google.com").asJson();
        assertTrue(createResponse.getCode() == 200);
        String shortUrl = createResponse.getBody().getObject().getJSONObject("link").getString("shortUrl");
        assertTrue(shortUrl != null && shortUrl.length() > 0);

        String id = createResponse.getBody().getObject().getJSONObject("link").getString("id");
        assertTrue("Response should contain link ID", id != null && id.length() > 2);

        String viewUrl = createResponse.getBody().getObject().getJSONObject("link").getString("viewUrl");
        assertTrue("Response should contain viewUrl", viewUrl != null && viewUrl.length() > 0);

        HttpResponse<String> targetPageResponse = Unirest.get(shortUrl).asString();
        assertTrue(targetPageResponse.getCode() == 200);
        assertTrue(targetPageResponse.getBody().contains("<title>Google</title>"));
    }

    @Test
    public void createLinkAndOpenViewPage() throws UnirestException {
        HttpResponse<JsonNode> createResponse = Unirest.post("http://localhost:9090/api/link/www.google.com").asJson();
        String viewUrl = createResponse.getBody().getObject().getJSONObject("link").getString("viewUrl");

        HttpResponse<String> viewPageResponse = Unirest.get(viewUrl).asString();
        assertTrue("ViewUrl should load: " + viewUrl, viewPageResponse.getCode() == 200);
    }
}
