package com.github.atok.UndertowRestHandler.reflection;

import com.github.atok.UndertowRestHandler.Utils;
import com.sun.jersey.api.uri.UriTemplate;
import io.undertow.util.HttpString;

import javax.ws.rs.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class RestRoute {
    public final UriTemplate uriTemplate;
    public final HttpString httpMethod;

    public final Method methodToCall;
    public final AnnotatedParam[] methodParams;

    public final Object resourceObject;

    public RestRoute(Object resourceObject, UriTemplate uriTemplate, String httpMethod, Method methodToCall, AnnotatedParam[] methodParams) {
        this.uriTemplate = uriTemplate;
        this.methodParams = methodParams;
        this.httpMethod = HttpString.tryFromString(httpMethod);
        this.methodToCall = methodToCall;
        this.resourceObject = resourceObject;
    }

    public static List<RestRoute> fromObject(Object resourceObject) {
        List<RestRoute> returnedList = new ArrayList<>();

        Path pathAnnotation = resourceObject.getClass().getAnnotation(Path.class);
        String resourcePath;
        if(pathAnnotation == null) {
            throw new IllegalArgumentException("Object have to be annotated with Path");
        }

        resourcePath = Utils.ensureStartingSlash(pathAnnotation.value());
        for (Method method : resourceObject.getClass().getMethods()) {

            Path methodPathAnnotation = method.getAnnotation(Path.class);
            String templateString;

            //FIXME not sure about that. Some URI builder would be useful.
            if(methodPathAnnotation != null) {
                templateString = Utils.ensureEndingSlash(resourcePath) + Utils.ensureNoStartingSlash(methodPathAnnotation.value());
            } else {
                templateString = resourcePath;
            }

            AnnotatedParam[] methodParams = new AnnotatedParam[method.getParameterCount()];
            for(int i = 0; i < method.getParameterCount(); i++) {
                methodParams[i] = new AnnotatedParam(method.getParameterAnnotations()[i], method.getParameterTypes()[i]);
            }

            UriTemplate uriTemplate = new UriTemplate(templateString);

            if(method.getAnnotation(GET.class) != null) {
                returnedList.add(new RestRoute(resourceObject, uriTemplate, HttpMethod.GET, method, methodParams));
            }

            if(method.getAnnotation(DELETE.class) != null) {
                returnedList.add(new RestRoute(resourceObject, uriTemplate, HttpMethod.DELETE, method, methodParams));
            }

            if(method.getAnnotation(POST.class) != null) {
                returnedList.add(new RestRoute(resourceObject, uriTemplate, HttpMethod.POST, method, methodParams));
            }

            if(method.getAnnotation(PUT.class) != null) {
                returnedList.add(new RestRoute(resourceObject, uriTemplate, HttpMethod.PUT, method, methodParams));
            }

            if(method.getAnnotation(HEAD.class) != null) {
                returnedList.add(new RestRoute(resourceObject, uriTemplate, HttpMethod.HEAD, method, methodParams));
            }
        }

        return returnedList;
    }

    @Override
    public String toString() {
        return "ResourceMapping{" +
                "uriTemplate=" + uriTemplate +
                ", httpMethod=" + httpMethod +
                ", methodToCall=" + methodToCall +
                ", resourceObject=" + resourceObject +
                '}';
    }
}
