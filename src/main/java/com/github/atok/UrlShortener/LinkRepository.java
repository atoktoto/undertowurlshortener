package com.github.atok.UrlShortener;


import com.github.atok.UrlShortener.model.Link;
import com.github.atok.UrlShortener.model.StoredLink;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LinkRepository {

    private final Map<String, StoredLink> storageMap;
    private final String shortUrlBase;
    private final String viewUrlBase;

    public LinkRepository(Map<String, StoredLink> storageMap, String shortUrlBase, String viewUrlBase) {
        this.storageMap = storageMap;
        this.shortUrlBase = shortUrlBase;
        this.viewUrlBase = viewUrlBase;
    }

    public Link createLink(String targetUrl) {
        String id = RandomStringUtils.randomAlphanumeric(6);
        String secret = RandomStringUtils.randomAlphanumeric(12);
        StoredLink storedLink = new StoredLink(targetUrl, id, secret, 0);
        storageMap.put(storedLink.id, storedLink);
        return new Link(storedLink, getShortUrlBase(), getViewUrlBase());
    }

    public Link countClick(Link link) {
        link.stored.clickCount++;
        storageMap.put(link.stored.id, link.stored);
        return link;
    }

    public Link getLink(String id) {
        StoredLink storedLink = storageMap.get(id);
        if(storedLink == null) {
            return null;
        } else {
            return new Link(storedLink, getShortUrlBase(), getViewUrlBase());
        }
    }

    public List<Link> getAll() {
        return storageMap.entrySet().stream().map(
                entry -> new Link(entry.getValue(), getShortUrlBase(), getViewUrlBase())
        ).collect(Collectors.toList());
    }

    public String getShortUrlBase() {
        return shortUrlBase;
    }

    public String getViewUrlBase() {
        return viewUrlBase;
    }

}
