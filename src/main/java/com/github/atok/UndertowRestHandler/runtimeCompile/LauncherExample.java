package com.github.atok.UndertowRestHandler.runtimeCompile;

public class LauncherExample implements CompiledRouteLauncher {
    final com.github.atok.UrlShortener.resources.LinkResource resource;

    public LauncherExample(Object resource) {
        this.resource = (com.github.atok.UrlShortener.resources.LinkResource)resource;
    }

    @Override
    public Object execute(Object... params) throws Exception {
        return resource.getLink((io.undertow.server.HttpServerExchange)params[0], (String) params[1]);
    }
}

